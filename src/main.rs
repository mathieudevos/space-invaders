mod enemy;
mod player;

use std::collections::HashSet;

use bevy::{prelude::*, sprite::collide_aabb::collide};
use enemy::EnemyPlugin;
use player::PlayerPlugin;

const PLAYER_SPRITE: &str = "player_a_01.png";
const LASER_PLAYER_SPRITE: &str = "laser_a_01.png";
const ENEMY_SPRITE: &str = "enemy_a_01.png";
const EXPLOSION_SPRITE: &str = "explo_a_sheet.png";
const LASER_ENEMY_SPRITE: &str = "laser_b_01.png";
const TIME_STEP: f32 = 1.0 / 60.0;
const SCALE: f32 = 0.5;
const PLAYER_RESPAWN_DELAY: f64 = 2.;
const MAX_ENEMIES: u32 = 5;
const MAX_FORMATION_MEMBERS: u32 = 2;

pub struct Sprites {
    player: Handle<Image>,
    laser_player: Handle<Image>,
    laser_enemy: Handle<Image>,
    enemy: Handle<Image>,
    explosion: Handle<TextureAtlas>,
}

struct WindowSize {
    w: f32,
    h: f32,
}

struct ActiveEnemies(u32);

#[derive(Component)]
struct PlayerState {
    alive: bool,
    death: f64,
}
impl Default for PlayerState {
    fn default() -> Self {
        Self {
            alive: false,
            death: 0.,
        }
    }
}
impl PlayerState {
    fn die(&mut self, time: f64) {
        self.alive = false;
        self.death = time;
    }
    fn spawn(&mut self) {
        self.alive = true;
        self.death = 0.;
    }
}

#[derive(Component)]
struct Player;

#[derive(Component)]
struct FromPlayer;

#[derive(Component)]
struct PlayerReadyFire(bool);

#[derive(Component)]
struct Laser;

#[derive(Component)]
struct Speed(f32);
impl Default for Speed {
    fn default() -> Self {
        Self(300.0)
    }
}

#[derive(Component)]
struct Enemy;

#[derive(Component)]
struct FromEnemy;

#[derive(Component)]
struct Explosion;

#[derive(Component)]
struct ExplosionToSpawn(Vec3);

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .insert_resource(WindowDescriptor {
            title: "A V A R U U S   _   I N V A D E R S".to_string(),
            width: 640f32,
            height: 800f32,
            ..Default::default()
        })
        .insert_resource(ActiveEnemies(0))
        .add_plugins(DefaultPlugins)
        .add_plugin(PlayerPlugin)
        .add_plugin(EnemyPlugin)
        .add_startup_system(setup)
        .add_system(laser_hit_enemy)
        .add_system(laser_hit_ferris)
        .add_system(explosion_to_spawn)
        .add_system(animate_explosion)
        .run();
}

fn setup(
    mut commands: Commands,
    assets: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut windows: ResMut<Windows>,
) {
    // Generate the window
    let window = windows.get_primary_mut().unwrap();

    // Camera
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    // Create main resources
    let texture_atlas =
        TextureAtlas::from_grid(assets.load(EXPLOSION_SPRITE), Vec2::new(64., 64.), 4, 4);
    commands.insert_resource(Sprites {
        player: assets.load(PLAYER_SPRITE),
        laser_player: assets.load(LASER_PLAYER_SPRITE),
        laser_enemy: assets.load(LASER_ENEMY_SPRITE),
        enemy: assets.load(ENEMY_SPRITE),
        explosion: texture_atlases.add(texture_atlas),
    });
    commands.insert_resource(WindowSize {
        w: window.width(),
        h: window.height(),
    });

    // Position window top right (1905, 5)
    window.set_position(IVec2::new(5, 5));
}

fn laser_hit_enemy(
    mut commands: Commands,
    laser_query: Query<(Entity, &Transform), (With<Laser>, With<FromPlayer>)>,
    enemy_query: Query<(Entity, &Transform), With<Enemy>>,
    sprites: Res<Sprites>,
    assets: Res<Assets<Image>>,
    mut active_enemies: ResMut<ActiveEnemies>,
) {
    let mut enemies_killed: HashSet<Entity> = HashSet::new();

    for (laser, laser_tf) in laser_query.iter() {
        for (enemy, enemy_tf) in enemy_query.iter() {
            let laser_image = assets.get(sprites.laser_player.clone_weak()).unwrap();
            let enemy_image = assets.get(sprites.enemy.clone_weak()).unwrap();
            let laser_size = laser_image.texture_descriptor.size;
            let enemy_size = enemy_image.texture_descriptor.size;

            let laser_scale = Vec2::from(laser_tf.scale.truncate());
            let enemy_scale = Vec2::from(enemy_tf.scale.truncate());
            let collision = collide(
                laser_tf.translation,
                Vec2::from((laser_size.width as f32, laser_size.height as f32)) * laser_scale,
                enemy_tf.translation,
                Vec2::from((enemy_size.width as f32, enemy_size.height as f32)) * enemy_scale,
            );

            if let Some(_) = collision {
                if enemies_killed.get(&enemy).is_none() {
                    commands.entity(enemy).despawn();
                    active_enemies.0 -= 1;

                    commands
                        .spawn()
                        .insert(ExplosionToSpawn(enemy_tf.translation.clone()));

                    enemies_killed.insert(enemy);
                }

                commands.entity(laser).despawn();
            }
        }
    }
}

fn laser_hit_ferris(
    mut commands: Commands,
    laser_query: Query<(Entity, &Transform), (With<Laser>, With<FromEnemy>)>,
    player_query: Query<(Entity, &Transform), With<Player>>,
    sprites: Res<Sprites>,
    assets: Res<Assets<Image>>,
    mut player_state: ResMut<PlayerState>,
    time: Res<Time>,
) {
    for (laser, laser_tf) in laser_query.iter() {
        for (player, player_tf) in player_query.iter() {
            let laser_image = assets.get(sprites.laser_enemy.clone_weak()).unwrap();
            let player_image = assets.get(sprites.player.clone_weak()).unwrap();
            let laser_size = laser_image.texture_descriptor.size;
            let player_size = player_image.texture_descriptor.size;

            let laser_scale = Vec2::from(laser_tf.scale.truncate());
            let player_scale = Vec2::from(player_tf.scale.truncate());
            let collision = collide(
                laser_tf.translation,
                Vec2::from((laser_size.width as f32, laser_size.height as f32)) * laser_scale.abs(),
                player_tf.translation,
                Vec2::from((player_size.width as f32, player_size.height as f32))
                    * player_scale.abs(),
            );

            if let Some(_) = collision {
                commands.entity(player).despawn();
                commands.entity(laser).despawn();
                player_state.die(time.seconds_since_startup());

                commands
                    .spawn()
                    .insert(ExplosionToSpawn(player_tf.translation.clone()));
            }
        }
    }
}

fn explosion_to_spawn(
    mut commands: Commands,
    query: Query<(Entity, &ExplosionToSpawn)>,
    assets: Res<Sprites>,
) {
    for (explosion_entity, explosion) in query.iter() {
        commands
            .spawn_bundle(SpriteSheetBundle {
                texture_atlas: assets.explosion.clone(),
                transform: Transform {
                    translation: explosion.0,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Explosion)
            .insert(Timer::from_seconds(0.05, true));

        commands.entity(explosion_entity).despawn();
    }
}

fn animate_explosion(
    mut commands: Commands,
    time: Res<Time>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    mut query: Query<
        (
            Entity,
            &mut Timer,
            &mut TextureAtlasSprite,
            &Handle<TextureAtlas>,
        ),
        With<Explosion>,
    >,
) {
    for (entity, mut timer, mut sprite, texture_atlas_handle) in query.iter_mut() {
        timer.tick(time.delta());
        if timer.finished() {
            let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
            sprite.index += 1;
            if sprite.index == texture_atlas.textures.len() {
                commands.entity(entity).despawn();
            }
        }
    }
}
