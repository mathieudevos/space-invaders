use bevy::{core::FixedTimestep, prelude::*};

use crate::{
    FromPlayer, Laser, Player, PlayerReadyFire, PlayerState, Speed, Sprites, WindowSize,
    PLAYER_RESPAWN_DELAY, SCALE, TIME_STEP,
};

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(PlayerState::default())
            .add_startup_stage("game_setup_actors", SystemStage::single(player_spawn))
            .add_system(player_movement)
            .add_system(player_fire)
            .add_system(laser_movement)
            .add_system_set(
                SystemSet::new()
                    .with_run_criteria(FixedTimestep::step(0.5))
                    .with_system(player_spawn),
            );
    }
}

fn player_spawn(
    mut commands: Commands,
    assets: Res<Sprites>,
    window_size: Res<WindowSize>,
    mut player_state: ResMut<PlayerState>,
    time: Res<Time>,
) {
    let now = time.seconds_since_startup();
    let death_time = player_state.death;

    if !player_state.alive && (death_time == 0. || now > death_time + PLAYER_RESPAWN_DELAY) {
        // Spawn sprite
        let bottom = -window_size.h / 2f32;
        commands
            .spawn_bundle(SpriteBundle {
                texture: assets.player.clone(),
                transform: Transform {
                    translation: Vec3::new(0.0, bottom + 75.0 / 4.0 + 5.0, 10.0),
                    scale: Vec3::new(SCALE, SCALE, 1.0),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Player)
            .insert(PlayerReadyFire(true))
            .insert(Speed::default());
        player_state.spawn();
    }
}

fn player_movement(
    keyboard_input: Res<Input<KeyCode>>,
    win_size: Res<WindowSize>,
    mut query: Query<(&Speed, &mut Transform), With<Player>>,
) {
    let query_output = query.get_single_mut();
    let (speed, mut transform) = match query_output {
        Ok(query_result) => query_result,
        Err(_) => return,
    };
    let dir = if keyboard_input.pressed(KeyCode::A)
        && transform.translation.x > -win_size.w / 2.0 + 144.0 / 4.0
    {
        -1.0
    } else if keyboard_input.pressed(KeyCode::D)
        && transform.translation.x < win_size.w / 2.0 - 144.0 / 4.0
    {
        1.0
    } else {
        0.0
    };
    transform.translation.x += dir * speed.0 * TIME_STEP;
}

fn player_fire(
    mut commands: Commands,
    kb: Res<Input<KeyCode>>,
    assets: Res<Sprites>,
    mut query: Query<(&Transform, &mut PlayerReadyFire), With<Player>>,
) {
    let query_output = query.get_single_mut();
    let (player_tf, mut ready_fire) = match query_output {
        Ok(query_result) => query_result,
        Err(_) => return,
    };
    if ready_fire.0 && kb.pressed(KeyCode::Space) {
        let x = player_tf.translation.x;
        let y = player_tf.translation.y;

        let mut spawn_lasers = |x_offset| {
            commands
                .spawn_bundle(SpriteBundle {
                    texture: assets.laser_player.clone(),
                    transform: Transform {
                        translation: Vec3::new(x + x_offset, y + 15.0, 0.0),
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(Laser)
                .insert(FromPlayer)
                .insert(Speed::default());
        };
        let x_offset = 144.0 / 4.0 - 5.0;

        spawn_lasers(x_offset);
        spawn_lasers(-x_offset);
        ready_fire.0 = false;
    }

    if kb.just_released(KeyCode::Space) {
        ready_fire.0 = true;
    }
}

fn laser_movement(
    mut commands: Commands,
    win_size: Res<WindowSize>,
    mut query: Query<(Entity, &Speed, &mut Transform), (With<Laser>, With<FromPlayer>)>,
) {
    for (laser, speed, mut laser_tf) in query.iter_mut() {
        let translation = &mut laser_tf.translation;
        translation.y += speed.0 * TIME_STEP;
        if translation.y > win_size.h {
            commands.entity(laser).despawn();
        }
    }
}
